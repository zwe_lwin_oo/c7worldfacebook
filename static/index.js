fetch('static/worldl.json')
.then(r=>r.json())
.then(r=>{
    for(let c of r){
        let d= document.createElement('div');
        d.innerText=c.name;
        d.classList.add('countryIndex');
        d.onclick=()=>{
            document.getElementById('content').innerHTML = ` 
            <h1>${c.name}</h1>
            <table>
            <tr><th>Country id : </th><td>${c.id}</td></tr>
            <tr><th>Capital : </th><td>${c.capital}</td></tr>
            <tr><th>Continent : </th><td>${c.continent}</td></tr>
            <tr><th>Area : </th><td>${c.area}</td></tr>
            <tr><th>Population : </th><td>${c.population}</td></tr>
            <tr><th>GDP : </th><td>${c.gdp}</td></tr>
            <tr><td colspan=2><img src=${c.flag}></td></tr>
            </table>
            
            
            `;
            let deleteButton = document.createElement('button');
            deleteButton.innerText = 'delete';
            document.getElementById('content').append(deleteButton);
            deleteButton.onclick = ()=>{
                fetch(`api/country/${c.id}`,{method:'DELETE'})
            }
        }
        document.getElementById('countryList').append(d);
    }

    //Implement Alphabet buttons
    for (let button of document.querySelectorAll('#alphabet button')){
        button.onclick=()=>{
            for(let n of document.querySelectorAll('#countryList div')){
                if(n.innerText.startsWith(button.innerText)){
                    n.classList.remove('hide');
                }else{
                    n.classList.add('hide');
                }
                
            }
        }
    }


    //Fill in dropdown
    var conlist=[];
    for(let c of r)
    {
        conlist.push(c.continent)
    }
    unitcon = new Set(conlist)
    console.log(unitcon);
    var option;
    for(let i of unitcon) { 
        console.log(i); 
        option = document.createElement("option");
        option.text = i;
        option.value=i;
        document.getElementById('dd').append(option);
        
    }
    document.querySelector('select').onchange=()=>{
        console.log(document.querySelector("select").value)
        document.getElementById('countryList').innerHTML="";
           let letter = document.querySelector('select').value;


           for(let c of r){
               if(letter===c.continent){

               
            let d= document.createElement('div');
            d.innerText=c.name;
            d.classList.add('countryIndex');
            d.onclick=()=>{
                document.getElementById('content').innerHTML = ` 
                <h1>${c.name}</h1>
                <table>
                <tr><th>Country id : </th><td>${c.id}</td></tr>
                <tr><th>Capital : </th><td>${c.capital}</td></tr>
                <tr><th>Continent : </th><td>${c.continent}</td></tr>
                <tr><th>Area : </th><td>${c.area}</td></tr>
                <tr><th>Population : </th><td>${c.population}</td></tr>
                <tr><th>GDP : </th><td>${c.gdp}</td></tr>
                <tr><td colspan=2><img src=${c.flag}></td></tr>
                </table>
                
                
                `;
                let deleteButton = document.createElement('button');
                deleteButton.innerText = 'delete';
                document.getElementById('content').append(deleteButton);
                deleteButton.onclick = ()=>{
                    fetch(`api/country/${c.id}`,{method:'DELETE'})
                }
            }
            document.getElementById('countryList').append(d);
        }

    }



           for(let n of document.querySelectorAll('#countryList')){
            if (n.innerText.startsWith(letter)){
                n.classList.remove('hide');
            }else{
                n.classList.add('hide');
            }
        } 
       }  
}
)